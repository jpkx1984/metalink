@echo off

WHERE lein > nul 2> nul

IF %ERRORLEVEL% EQU 0 (
	lein uberjar && lein resource
) ELSE (
	ECHO "Nalezy zainstalowac Leiningena: https://leiningen.org"
)
