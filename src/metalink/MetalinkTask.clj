(ns metalink.MetalinkTask
  (:import [org.apache.tools.ant.types FileSet]
           [org.apache.tools.ant.types.resources FileResource]
           [java.io ByteArrayOutputStream]
           [java.security MessageDigest]
           [java.text DateFormat SimpleDateFormat])
  (:require [clojure.data.xml :as xml]
            [clojure.java.io :as io])
  (:gen-class
   :extends workaround.BaseTask
   :state state
   :init my-init
   :constructors {[] []}
   :prefix "ml-"; przedrostem dla metod generowanej klasy
   :main false
   :exposes-methods {execute superExecute}
   :methods [[addFileset [org.apache.tools.ant.types.FileSet] void]
             [setFile [String] void]
             [getFile [] String]
             [setUrl [String] void]
             [getUrl [] String]]))


(defn ml-my-init []
  "Zainicjuj stan taska jako słownik."
  [[] (atom {::filesets []})])


(defn- setfield [this key value]
  "Ustaw wartość pola."
  (swap! (.state this) into {key value}))

(defn- getfield [this key]
  "Pobierz wartość pola."
  (@(.state this) key))

(defn ml-addFileset [this fs]
  (let [t (or (getfield this ::filesets) [])]
    (setfield this ::filesets
              (conj t fs))))

(defn ml-setFile [this file-name]
  (setfield this ::file-name file-name))

(defn ml-getFile [this]
  (getfield this ::file-name))

(defn ml-setUrl [this url-name]
  (setfield this ::url-name url-name))

(defn ml-getUrl [this]
  (getfield this ::url-name))

(def md5-instance (MessageDigest/getInstance "MD5"))

(defn md5 [b]
  "MD5 z tablicy bajtów."
  (let [raw (.digest md5-instance b)]
    (format "%032x" (BigInteger. 1 raw))))

(def ^:dynamic log (fn [s] s))

(defn- resource-to-node [r url-name]
  "Zamienia zasób na węzeł typu file."
  (let [f (.getFile r)]
    (with-open [data-out (ByteArrayOutputStream.)
                data-in (.getInputStream r)]
      (io/copy data-in data-out)
      (log (.getName r))
      (let [data (.toByteArray data-out)]
        [:file {:name (.getName f)}
         [:size {} (count data)]
         [:hash {:type "md5"} (md5 data)]
         [:url {} (str url-name (.getName r))]]))))

(defn fix-url [s]
  "Poprawia URL."
  (if-not (.endsWith s "/")
    (str s "/")
    s))

(defn ml-execute [this]
  (binding [log (fn [s] (.log this s))]
    (let [pub-date (java.util.Date/from (java.time.Instant/now))
          date-format (SimpleDateFormat. "yyyy-MM-dd'T'HH:mm:ss'Z'")
          out-file-name (or
                         (.getFile this)
                         "metalink.xml")
          url-name (fix-url
                    (or (.getUrl this)
                        (.. this (getProject) (getProperty "server.files.url"))
                        ""))
          filesets (seq (or (getfield this ::filesets) []))
          document [:metalink {:xmlns "urn:ietf:params:xml:ns:metalink"}
                    [:published {} (.format date-format pub-date)]]
          file-entries (for [fs filesets
                             r (seq fs)
                             :when (and (instance? FileResource r)
                                        (not (.isDirectory r))
                                        (.isExists r)
                                        (.. r (getFile) (canRead)))]
                         (resource-to-node r url-name))]
      (.superExecute this)
      ;; wypluj XMLa
      (spit out-file-name
            (xml/indent-str
             (xml/sexp-as-element
              (vec (concat document file-entries)))))
      (.log this "Hello"))))
