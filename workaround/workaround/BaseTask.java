package workaround;


/**
 * Obejście problemu z class loaderem bazowych bibliotek Clojure.
 */
public class BaseTask extends org.apache.tools.ant.Task
{
	static
	{
		Thread.currentThread()
			.setContextClassLoader(BaseTask.class.getClassLoader());
	}
}
