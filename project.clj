(defproject metalink "0.1.0-SNAPSHOT"
  :description "Metalink - projekt zaliczeniowy. Janusz Krysztofiak (69622)"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.xml "0.0.8"]]
  :plugins [[lein-resource "16.9.1"]]
  :resource {:resource-paths [["target" {:includes [#".+standalone.jar$"]}]]
             :target-path "testbed/libs"
             :skip-stencil [#".*"]}
  ;:prep-tasks ["javac" "compile"]

  ;; Docelowo nie będziemy umieszczać biblioteki Anta
  :profiles {:provided
             {:dependencies [[org.apache.ant/ant "1.9.7"]]}}
  :jvm-opts ["-Dclojure.compiler.direct-linking=true"]
  :source-paths ["src"]
  :java-source-paths ["workaround/workaround"]
  :aot :all)
