#!/bin/bash


command -v lein >/dev/null 2>&1 || { echo "Należy zainstalować Leiningena: https://leiningen.org" >&2; exit 1; }

lein uberjar && lein resource
