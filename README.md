# metalink

Task anta napisany w Clojure, generujący plik metalink.

## Użytkowanie

Fire and forget.

Zalecana wersja Javy: 1.8.0_60+

## Struktura

doc - dokumentacja
resources - zasoby
src - kod źródłowy taka w Clojure
target - skompilowane klasy i JARy
testbed - podkatalog do sprawdzania taska
workaround - workaround problemu z class loaderem

## License

Copyright © 2018 Janusz Krysztofiak

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
